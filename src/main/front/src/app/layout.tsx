import './globals.css'
import {ReactNode} from "react";

export const metadata = {
  title: 'Tienda Online',
  description: 'Tienda de ropa  exclusa en medellin',
}

function RootLayout({
  children,
}: {
  children: ReactNode
}) {
  return (
    <html lang="en">
      <body>
        {children}
        </body>
    </html>
  )
}

export default  RootLayout;