import ClientComponent from "../../../Common/Client/UseClient";
import HomeStore from "../../../modules/Shop/infrastructure/Pages/HomeStore";
export default function Home() {
	return (
		<ClientComponent>
		  <HomeStore />
		</ClientComponent>
	  );
}
