import {Navbar} from "../../../Common/Navbar";
import {ReactNode} from "react";

export default function Layout({
   children,
}: {
    children: ReactNode
}) {
    return (
        <main>
            <Navbar/>
            {children}
        </main>
    )
}