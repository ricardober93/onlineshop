export interface Product {
    name: string;
    cost: number;
    description: string;
    imageUrl: string;   
    id: string;
    price: number;
    quantity: number;
    category: string;
    brand: string;
}