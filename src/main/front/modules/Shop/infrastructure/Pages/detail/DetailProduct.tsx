import Image from "next/image";
import { Product } from "../../../domain/Product.model";
import HeaderDetailProduct from "../../Components/Header/HeaderDetailProduct";

type Props = {
	product?: Product;
};

function DetailProduct({ product }: Props) {
 
	return (
		<main className="pt-8 px-4">
			<HeaderDetailProduct title="Lörem ipsum vun demitopi vilogi respektive rande intral afunt. |" />

			<Image
				width={480}
				height={480}
				className="w-full rounded-lg bg-cover"
				src="https://images.unsplash.com/photo-1604537529428-15bcbeecfe4d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1469&q=80"
				alt="ropa de mujer"
			/>
			<section className="grid grid-rows-2 gap-1 mt-8">
				<h6 className="text-lg text-blue-800 font-extrabold">COP 14.000</h6>
				<h6 className="text-md text-gray-500 line-through">COP 24.000</h6>
			</section>
			<section className="mt-4">
				<p className="text-md font-normal">
					Lörem ipsum bioryren vavis. Gigang fagt sedan fanytrengar nihaledes
					dites. Laliga dosam dohaktig. Plare vire näling exosade dälingar. Itt
					ände krock retailtainment, anabelt.{" "}
				</p>
			</section>
			<hr className="border my-5" />
			<section className="flex flex-col gap-2">
				<h1 className="text-lg text-blue-800 font-medium"> Entrega </h1>
				<div className="grid">
					<h6 className="text-md text-green-800 font-medium">Envio gratis</h6>
					<p>
						Se entraga el <span className="font-bold"> 18 de abril</span>{" "}
					</p>
				</div>
			</section>
			<button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg text-lg w-full mt-3">
				Comprar
			</button>
		</main>
	);
}

export default DetailProduct;
