import CardProduct from "../Components/CardProduct";
import Categories from "../Components/Categories/Categories";
import ListProduct from "../Components/Product/ListProduct";

export default function HomeStore() {
	return (
		<main className="px-2 dark:bg-zinc-800">
			<section id="categories">
				<h1 className="text-2xl font-bold">Selecciona tu categoria</h1>
				<Categories />
			</section>
				<CardProduct />
				<ListProduct />
		</main>
	);
}
