"use client";
import Image from "next/image";
import {
	useEffect,
	useState
} from "react";

export default function CardProduct() {
	const [active, setActive] = useState(0);
	const [newProduct, setNewProduct] = useState([
		{
			id: "345",
			urlImage:
				"https://images.unsplash.com/photo-1604537529428-15bcbeecfe4d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1469&q=80",
			name: "Ropa De mujer Ultima Moda",
		},
		{
			id: "53453",
			urlImage:
				"https://images.unsplash.com/photo-1670272501077-c72d2d42f362?ixlib=rb-4.0.3&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",
			name: "Ropa De mujer Ultima Moda",
		},
		{
			id: "345345",
			urlImage:
				"https://images.unsplash.com/photo-1679730297595-b4778686e953?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2065&q=80",
			name: "Ropa De mujer Ultima Moda",
		},
	]);
	let scrollInterval: NodeJS.Timeout;
	const onChangeImage = (index: number) => {
		setActive(index);
	};

	const previus = () => {
		const value = active -1 < 0 ? newProduct.length -1 : active-1;
		setActive((value));
	};

	const next = () => {
		const value = active +1 > newProduct.length -1 ? 0 : active+1;
		setActive(value);
	};




	useEffect(() => {
		scrollInterval = setTimeout(() => {
			const value = active +1 > newProduct.length -1 ? 0 : active+1;
			setActive(value);
		  }, 2000);
		  return () => clearTimeout(scrollInterval);
	}, [])
	
	return (
		<section className="relative w-full">
			<div className="relative h-56 overflow-hidden rounded-lg sm:h-64 xl:h-80 2xl:h-96">
				{newProduct.map((product, index) => (
					<div id={`carousel-${index}`} key={`carousel-${index+1}`} className={`${ active === index ? 'visible' : 'hidden'} duration-700 ease-in-out`}>
						<Image
							width={480}
							height={380}
							src={product.urlImage}
							className="absolute block w-full -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2"
							alt={product.name}
						/>
					</div>
				))}
			</div>
			<div className="absolute z-10 flex space-x-3 -translate-x-1/2 bottom-5 left-1/2">
				{newProduct.map((product, index) => (
					<button
						key={`carousel-indicator-${index}`}
						type="button"
						className={`${ active === index ? '' : 'opacity-75'} w-3 h-3 rounded-full bg-gray-50 `}
						aria-current="true"
						aria-label={`Slide-${index}`}
						onClick={() => onChangeImage(index)}
					>
						<span className="sr-only">1</span>
					</button>
				))}
			</div>
			<button
				onClick={previus}
				id="data-carousel-prev"
				type="button"
				className="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
			>
				<span className="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
					<svg
						className="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800"
						fill="none"
						stroke="currentColor"
						viewBox="0 0 24 24"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							strokeLinecap="round"
							strokeLinejoin="round"
							strokeWidth="2"
							d="M15 19l-7-7 7-7"
						/>
					</svg>
					<span className="hidden">Previous</span>
				</span>
			</button>
			<button
			onClick={next}
				id="data-carousel-next"
				type="button"
				className="absolute top-0 right-0 z-10 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
			>
				<span className="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
					<svg
						className="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800"
						fill="none"
						stroke="currentColor"
						viewBox="0 0 24 24"
						xmlns="http://www.w3.org/2000/svg"
					>
						<path
							strokeLinecap="round"
							strokeLinejoin="round"
							strokeWidth="2"
							d="M9 5l7 7-7 7"
						/>
					</svg>
					<span className="hidden">Next</span>
				</span>
			</button>
		</section>
	);
}
