"use client";
import Image from "next/image";
import Link from "next/link";
type Props = {};

export default function ListProduct() {
	return (
		<section className="w-full grid justify-start mt-5">
			<h1 className='text-left text-xl font-bold'> Productos </h1>

			<article className="flex flex-col gap-2 mt-3">
				{[0, 1, 2].map((list, index) => (
					<Link
						key={index}
						href={`/detail/${index}`}
					>
						<div className="bg-blue-100 rounded-lg w-full flex justify-cetner items-start gap-2 p-4">
							<Image
								width={60}
								height={60}
								className="rounded-lg bg-cover mt-2"
								src="https://images.unsplash.com/photo-1604537529428-15bcbeecfe4d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1469&q=80"
								alt="ropa de mujer"
							/>

							<section className="flex flex-col gap-2">
								<h6 className="text-md">
									{" "}
									Lörem ipsum vun demitopi vilogi respektive rande intral afunt.{" "}
								</h6>
								<footer className="w-full flex justify-between items-center">
									<div className="grid gap-1">
										<h6 className="text-md text-blue-800 font-extrabold">
											COP 14.000
										</h6>
										<h6 className="text-xs text-gray-500 line-through">
											COP 24.000
										</h6>
									</div>
									<button className="text-lg">
										<svg
											xmlns="http://www.w3.org/2000/svg"
											fill="none"
											viewBox="0 0 24 24"
											strokeWidth={1.5}
											stroke="currentColor"
											className="w-6 h-6 text-blue-800"
										>
											<path
												strokeLinecap="round"
												strokeLinejoin="round"
												d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"
											/>
										</svg>
									</button>
								</footer>
							</section>
						</div>
					</Link>
				))}
			</article>
		</section>
	);
}
