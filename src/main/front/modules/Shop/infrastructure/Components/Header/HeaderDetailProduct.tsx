'use client';
import React from "react";
import ArrowBack from "../../../../../Common/Icons/ArrowBack";
import { useRouter } from 'next/navigation';

type Props = {
	title: string;
};

export default function HeaderDetailProduct({ title }: Props) {
	const router = useRouter();
	return (
		<header className='flex justify-start items-start w-full mb-5'>
			<button
				onClick={router.back}
				className='font-semibold  py-2 px-3  rounded'
			>
				<ArrowBack strokeWidth={1.5} />
			</button>
			<h1 className="font-bold text-xl leading-7">{title}</h1>
		</header>
	);
}
