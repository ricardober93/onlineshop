'use client';
import { useState } from "react";
import { Category } from "../../../domain/category.model";

export default function Categories() {
	const [categories, setCategories] = useState<Category[]>([
		{
			id: "1",
			name: "Todos",
		},
		{
			id: "2",
			name: "Mujer",
		},
		{
			id: "3",
			name: "Hombre",
		},
	]);
	return (
		<section className="flex gap-4 py-4 overflow-x-scroll">
			{categories.map((category) => (
				<div key={category.id} className="h-6 py-5 px-6 rounded-lg bg-gray-50 flex justify-center items-center">
					{category.name}
				</div>
			))}
		</section>
	);
}
