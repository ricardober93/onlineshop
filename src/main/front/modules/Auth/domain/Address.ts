export interface Address {
    city: string,
    name: string;
    states: string;
}