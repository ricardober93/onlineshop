import {UserId} from "./UserId";
import {Address} from "./Address";

export interface User {
    address: Address;
    id: UserId;
    lastName: string;
    name: string;
    phone: number,

}