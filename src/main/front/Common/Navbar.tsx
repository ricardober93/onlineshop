import Sidenav from "./Client/Sidenav/Sidenav";
import ShoppingCartIcon from "./Icons/ShoppingCartIcon";
import Notification from "./Client/Notifications/Notification";
import Link from "next/link";

type Props = {};

export const Navbar = (props: Props) => {
	return (
		<header className='px-2 py-4 w-full flex justify-between items-center dark:bg-zinc-800'>
			<Sidenav />
			<section className="flex justify-between gap-2">
				<Notification />
				<Link href="cart" className="flex items-center justify-center rounded-lg p-2 dark:text-neutral-200">
					<ShoppingCartIcon />
				</Link>
			</section>
		</header>
	);
};
