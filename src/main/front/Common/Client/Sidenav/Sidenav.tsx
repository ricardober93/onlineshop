"use client";
import Link from "next/link";
import { KeyboardEvent, useState } from "react";
import BarIcon from "../../Icons/BarIcon";
import CloseIcon from "../../Icons/CloseIcon";
import Home from "../../Icons/Home";
import ProfileIcon from "../../Icons/ProfileIcon";

export default function Sidenav() {
	const [isOpen, setIsOpen] = useState(false);
	const toogle = () => setIsOpen((prev) => !prev);
	const close = () => setIsOpen(false);

	const closeEsc = (event: KeyboardEvent<HTMLInputElement>) => {
		console.log(event)
		if (event	.code === "Escape") {
            close();
        }
	}

	return (
		<>
			<button
				onClick={toogle}
				className="p-2 font-medium leading-tight shadow-md hover:shadow-lg active:bg-blue-400 rounded-lg active:shadow-lg dark:text-slate-50"
			>
				<BarIcon />
			</button>
			<aside>
				<section
					onKeyDown={closeEsc}
					onClick={close}
					className={`${
						isOpen ? "" : "hidden"
					} absolute  top-0 left-0  z-20 bg-zinc-400/10 w-full h-full`}
				>
					<span className="sr-only"> backdrop</span>{" "}
				</section>
				<nav
					className={`${
						isOpen ? "translate-x-0" : "-translate-x-full"
					} absolute left-0 top-0 z-50 h-full  w-9/12 md:w-96 shadow-xl transition duration-150 ease-in-out overflow-hidden bg-white dark:bg-zinc-800`}
				>
					<ul className="relative z-50 m-0 list-none px-3 py-4 h-full dark:text-slate-50">
						<li className="w-full flex justify-end items-end">
							<button
								onClick={toogle}
								className=" shadow-md  cursor-pointer rounded p-2  hover:bg-slate-50 focus:bg-slate-50 active:bg-slate-50  dark:text-gray-300"
							>
								<CloseIcon />
							</button>
						</li>
						<li className="w-full flex flex-col gap-2 justify-start items-start my-4 py-2">
							<h1 className="text-xl font-semibold">Ricardo Bermudez</h1>
							<p className="text-base ">
								Calle 3 sur bis - 13 49, San Juan del Cesar. La Guajira
							</p>
							<hr className="w-full border-1 border-gray-1500 mt-4" />
						</li>

						<li className=" w-full grid gap-4 mt-7">
							<Link
								href=""
								onClick={close}
								className=" w-full flex  items-center justify-start gap-2 py-2"
							>
								{" "}
								<Home /> Home{" "}
							</Link>
							<Link
								href="profile/24354"
								onClick={close}
								className=" w-full flex  items-center justify-start gap-2 py-2"
							>
								{" "}
								<ProfileIcon /> Perfil{" "}
							</Link>
						</li>
					</ul>
				</nav>
			</aside>
		</>
	);
}
