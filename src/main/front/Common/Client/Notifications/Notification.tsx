"use client";

import { useState } from "react";
import NotificationIcon from "../../Icons/NotificationIcon";

export default function Notification() {
	const [isOpen, setIsOpen] = useState(false);
	const toogle = () => setIsOpen((prev) => !prev);
	const close = () => setIsOpen(false);
	return (
		<div className="relative">
			<div className="relative inline-flex w-fit">
				<div className="absolute bottom-0 left-0 right-auto top-auto z-10 inline-block -translate-x-2/4 translate-y-1/2 rotate-0 skew-x-0 skew-y-0 scale-x-100 scale-y-100 rounded-full bg-pink-700 p-1.5" />
				<div
					onKeyDown={toogle}
					onClick={toogle}
					className="cursor-pointer flex items-center justify-center rounded-lg p-2 shadow-lg dark:text-neutral-200"
				>
					<NotificationIcon />
				</div>
			</div>
			<ul
				className={`${
					isOpen ? "" : "hidden"
				} absolute z-50 right-0 float-right mt-2 min-w-max list-none overflow-hidden rounded-lg border-none bg-white text-right shadow-lg dark:bg-neutral-700`}
			>
				<li>
					<span
						className="block w-full whitespace-nowrap bg-transparent px-4 py-2 text-sm font-normal text-neutral-700 hover:bg-neutral-100 active:text-neutral-800 active:no-underline disabled:pointer-events-none disabled:bg-transparent disabled:text-neutral-400 dark:text-neutral-200 dark:hover:bg-neutral-600"
						data-te-dropdown-item-ref
					>
						Action
					</span>
				</li>
				<li>
					<span
						className="block w-full whitespace-nowrap bg-transparent px-4 py-2 text-sm font-normal text-neutral-700 hover:bg-neutral-100 active:text-neutral-800 active:no-underline disabled:pointer-events-none disabled:bg-transparent disabled:text-neutral-400 dark:text-neutral-200 dark:hover:bg-neutral-600"
						data-te-dropdown-item-ref
					>
						Another action
					</span>
				</li>
				<li>
					<span
						className="block w-full whitespace-nowrap bg-transparent px-4 py-2 text-sm font-normal text-neutral-700 hover:bg-neutral-100 active:text-neutral-800 active:no-underline disabled:pointer-events-none disabled:bg-transparent disabled:text-neutral-400 dark:text-neutral-200 dark:hover:bg-neutral-600"
						data-te-dropdown-item-ref
					>
						Something else here
					</span>
				</li>
			</ul>
		</div>
	);
}
